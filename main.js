window.onload = function () {
  const game = new Game()

  document.getElementById('fullscreen-button').onclick = function () {
    document.querySelector('canvas').requestFullscreen()
    this.blur() // Prevent Return from "clicking" this again.
  }

  document.getElementById('enable-audio-button').onclick = function () {
    game.enableAudio()
    this.blur() // Prevent Return from "clicking" this again.
  }

  game.mainLoop()
}
