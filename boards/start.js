// ----------------------------------------------------------------------------
if (!window.BOARDS) { window.BOARDS = {} }; window.BOARDS['start.json'] =
// ----------------------------------------------------------------------------
[
  { kind: 'goal', position: [128, 128], angle: 0, warpTo: 'bombs.json' },

  { kind: 'pizza', position: [256, 224], angle: 0 },
  { kind: 'pizza', position: [384, 224], angle: 0 },
  { kind: 'pizza', position: [320, 352], angle: 0 },

  { kind: 'wall', position: [64,  64], angle: 0 },
  { kind: 'wall', position: [96,  64], angle: 0 },
  { kind: 'wall', position: [128, 64], angle: 0 },
  { kind: 'wall', position: [160, 64], angle: 0 },
  { kind: 'wall', position: [192, 64], angle: 0 },

  { kind: 'wall', position: [448, 64], angle: 0 },
  { kind: 'wall', position: [480, 64], angle: 0 },
  { kind: 'wall', position: [512, 64], angle: 0 },
  { kind: 'wall', position: [544, 64], angle: 0 },
  { kind: 'wall', position: [576, 64], angle: 0 },

  { kind: 'wall', position: [64,  96], angle: 0 },
  { kind: 'wall', position: [64,  128], angle: 0 },
  { kind: 'wall', position: [64,  160], angle: 0 },
  { kind: 'wall', position: [64,  192], angle: 0 },
  { kind: 'wall', position: [64,  224], angle: 0 },
  { kind: 'wall', position: [64,  256], angle: 0 },
  { kind: 'wall', position: [64,  288], angle: 0 },
  { kind: 'wall', position: [64,  320], angle: 0 },
  { kind: 'wall', position: [64,  352], angle: 0 },
  { kind: 'wall', position: [64,  384], angle: 0 },
  { kind: 'wall', position: [64,  416], angle: 0 },

  { kind: 'wall', position: [96,  416], angle: 0 },
  { kind: 'wall', position: [128, 416], angle: 0 },
  { kind: 'wall', position: [160, 416], angle: 0 },
  { kind: 'wall', position: [192, 416], angle: 0 },
  { kind: 'wall', position: [224, 416], angle: 0 },
  { kind: 'wall', position: [256, 416], angle: 0 },
  { kind: 'wall', position: [288, 416], angle: 0 },
  { kind: 'wall', position: [320, 416], angle: 0 },
  { kind: 'wall', position: [352, 416], angle: 0 },
  { kind: 'wall', position: [384, 416], angle: 0 },
  { kind: 'wall', position: [416, 416], angle: 0 },
  { kind: 'wall', position: [448, 416], angle: 0 },

  { kind: 'wall', position: [192, 96], angle: 0 },
  { kind: 'wall', position: [192, 128], angle: 0 },
  { kind: 'wall', position: [192, 160], angle: 0 },

  { kind: 'wall', position: [192, 288], angle: 0 },
  { kind: 'wall', position: [224, 288], angle: 0 },
  { kind: 'wall', position: [256, 288], angle: 0 },
  { kind: 'wall', position: [288, 288], angle: 0 },
  { kind: 'wall', position: [320, 288], angle: 0 },
  { kind: 'wall', position: [352, 288], angle: 0 },
  { kind: 'wall', position: [384, 288], angle: 0 },
  { kind: 'wall', position: [416, 288], angle: 0 },
  { kind: 'wall', position: [448, 288], angle: 0 },

  { kind: 'wall', position: [224, 160], angle: 0 },
  { kind: 'wall', position: [256, 160], angle: 0 },
  { kind: 'wall', position: [288, 160], angle: 0 },
  { kind: 'wall', position: [320, 160], angle: 0 },
  { kind: 'wall', position: [352, 160], angle: 0 },
  { kind: 'wall', position: [384, 160], angle: 0 },
  { kind: 'wall', position: [416, 160], angle: 0 },
  { kind: 'wall', position: [448, 160], angle: 0 },

  { kind: 'wall', position: [576, 96], angle: 0 },
  { kind: 'wall', position: [576, 128], angle: 0 },
  { kind: 'wall', position: [576, 160], angle: 0 },
  { kind: 'wall', position: [576, 192], angle: 0 },
  { kind: 'wall', position: [576, 224], angle: 0 },
  { kind: 'wall', position: [576, 256], angle: 0 },
  { kind: 'wall', position: [576, 288], angle: 0 },
  { kind: 'wall', position: [576, 320], angle: 0 },
  { kind: 'wall', position: [576, 352], angle: 0 },
  { kind: 'wall', position: [576, 384], angle: 0 },
  { kind: 'wall', position: [576, 416], angle: 0 },

  { kind: 'player', position: [224, 112], angle: 0 }
]
