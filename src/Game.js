class Game {
  constructor () {
    this.input = new UserInput()
    this.graphics = new Graphics('canvas')
  }

  enableAudio () {
    if (this.audio) { return }
    this.audio = new Audio()
    this.audio.play('music', true)
  }

  mainLoop (currentTime) {
    requestAnimationFrame(this.mainLoop.bind(this))

    if (this.lastUpdate && this.lastUpdate > currentTime - 16) { return }
    this.lastUpdate = currentTime

    if (this.world) {
      this.world.update(this.input.getState(), this.audio, currentTime)
    } else if (this.input.getState().fire[0]) {
      this.world = new World()
      if (this.audio) { this.audio.play('start') }
    }

    this.graphics.update(this.world)
  }
}
