class Graphics {
  constructor (selector) {
    this.canvas = document.querySelector(selector)
    this.ctx = this.canvas.getContext('2d')

    this.images = {
      complete: document.getElementById('complete'),
      failed: document.getElementById('failed'),
      goal: document.getElementById('goal'),
      pizza: document.getElementById('pizza'),
      player: document.getElementById('player'),
      bomb: document.getElementById('bomb'),
      title: document.getElementById('title'),
      wall: document.getElementById('wall')
    }
  }

  update (world) {
    this.ctx.fillStyle = '#000088'
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

    if (world) {
      world.getActors().forEach(actor => {
        const image = this.images[actor.kind]
        if (!image || !image.naturalWidth) { return }

        this.ctx.save()
        this.ctx.translate(actor.position[0], actor.position[1])
        this.ctx.rotate(actor.angle)
        this.ctx.drawImage(image, -image.naturalWidth/2, -image.naturalHeight/2)
        this.ctx.restore()
      })

      if (world.status() === 'complete') {
        this._drawImage(this.images.complete)
      } else if (world.status() === 'failed') {
        this._drawImage(this.images.failed)
      }

    } else {
      this._drawImage(this.images.title)
    }
  }

  _drawImage (image) {
    if (image.naturalWidth) { this.ctx.drawImage(image, 0, 0) }
  }
}
