class Audio {
  constructor () {
    this.sounds = {
      music: document.getElementById('music'),
      bounce: document.getElementById('bounce'),
      bonus: document.getElementById('bonus'),
      death: document.getElementById('death'),
      start: document.getElementById('start'),
      win: document.getElementById('win')
    }
  }

  play (name, loop) {
    if (!this.sounds[name]) { return }

    this.sounds[name].currentTime = 0
    this.sounds[name].loop = !!loop
    this.sounds[name].play()
  }
}
