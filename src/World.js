class World {
  constructor () {
    this._load('start.json')
  }

  _load (boardName) {
    const json = window.BOARDS[boardName]

    this.actors = json.map(actor => {
      const actorCopy = { ...actor }
      actorCopy.position = actor.position.map(axis => axis)
      return actorCopy
    })

    this.levelName = boardName
    this.nextLevel = null
    this.dead = false
  }

  update (userInput, audio, currentTime) {
    if (this.nextLevel || this.dead) {
      if (userInput.fire[0]) {
        this._load(this.nextLevel ? this.nextLevel : this.levelName)
        if (audio) { audio.play('start') }
      }

      return
    }

    this.actors.forEach(actor => {
      if (actor.kind === 'pizza') {
        return actor.angle = Math.sin(currentTime * 0.01) * 0.5
      } else if (actor.kind !== 'player') {
        return
      }

      actor.angle += userInput.axis[0] * 0.05
      const s = userInput.fire[1] ? 8 : 4

      const velocity = [Math.cos(actor.angle) * s, Math.sin(actor.angle) * s]
      this._move(actor, velocity, audio)
    })

    this.actors = this.actors.filter(actor => !actor.destroy)
  }

  _move(actor, velocity, audio) {
    const boardSize = [640, 480]

    boardSize.forEach((maximum, axis) => {
      const oldPosition = actor.position[axis]
      actor.position[axis] += velocity[axis]

      // This is defined once, here, so as to avoid repetition below:
      const bounce = () => {
        actor.position[axis] = oldPosition
        actor.angle = (axis === 0 ? Math.PI : 0) - actor.angle
        if (audio) { audio.play('bounce') }
      }

      // Actor vs. other actor collision:
      this.actors.forEach(other => {
        if (!this._overlap(actor, other)) { return }
        else if (other.kind === 'wall') { bounce() }
        else if (other.kind === 'pizza') {
          if (audio) { audio.play('bonus') }
          other.destroy = true
        }
        else if (other.kind === 'goal' && other.warpTo) {
          if (audio) { audio.play('win') }
          this.nextLevel = other.warpTo
        }
        else if (other.kind === 'bomb') {
          if (audio) { audio.play('death') }
          this.dead = true
        }
      })

      // Actor vs. edge-of-screen collision:
      if (velocity[axis] < 0 && actor.position[axis] < 0) { bounce() }
      if (velocity[axis] > 0 && actor.position[axis] > maximum) { bounce() }
    })

    return null
  }

  _overlap (actor, other) {
    const HALF_SIZE = 16 // All sprites are 32x32
    return (actor !== other && !actor.destroy && !other.destroy &&
            actor.position[0] - HALF_SIZE < other.position[0] + HALF_SIZE &&
            actor.position[0] + HALF_SIZE > other.position[0] - HALF_SIZE &&
            actor.position[1] - HALF_SIZE < other.position[1] + HALF_SIZE &&
            actor.position[1] + HALF_SIZE > other.position[1] - HALF_SIZE)
  }

  getActors () {
    return this.actors
  }

  status () {
    if (this.nextLevel) { return 'complete' }
    return this.dead ? 'failed' : null
  }
}
